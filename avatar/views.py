from rest_framework import generics
from .models import Avatar
from .serializers import AvatarSerializer


class AvatarViewSet(generics.ListCreateAPIView):
    authentication_classes = ()
    permission_classes = ()
    model = Avatar
    queryset = Avatar.objects.all()
    serializer_class = AvatarSerializer
