from avatar.models import Avatar
from rest_framework import serializers

class AvatarSerializer(serializers.ModelSerializer):
    attributes = serializers.StringRelatedField(many=True)

    class Meta:
        model = Avatar
        fields = ['nickname', 'name', 'last_name', 'attributes']