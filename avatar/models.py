from django.db import models

class AttrCategory(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class AttrTypes(models.Model):
    name = models.CharField(max_length=50, default="")
    description = models.TextField()
    category = models.ForeignKey(AttrCategory, related_name="attr_type", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class Attribute(models.Model):
    name = models.CharField(max_length=50, default="")
    description = models.TextField()
    detail = models.TextField()
    attr_type = models.ForeignKey(AttrTypes, related_name="attribute", on_delete=models.CASCADE)

    def __str__(self):
        return "{} : {}".format(self.name, self.description)

class Avatar(models.Model):
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    nickname = models.CharField(max_length=50)
    attributes = models.ManyToManyField(Attribute, related_name='avatars')

    def __str__(self):
        return "{} : {} {}".format(self.nickname, self.name, self.last_name)