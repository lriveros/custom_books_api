# How to Setup?

Recommended step by step setup
### Virtualenv (optional) 
virtualenv is recomended to run this project
```
$ virtualenv venv
```

- Linux

```
$ source venv/Scripts/activate
```

- Windows
```
> venv\Scripts\activate.bat
```


### Install dependencies 
```
(venv) $ pip install -r requirements.txt
```

### Run Server
```bash
(venv) $ python manage.py runserver
```