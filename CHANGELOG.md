# Changelog
All notable changes to this project will be documented in this file.

## 0.0.1 - 2019-01-01
### Initial
* Initial Version
* Token and login control
* Database connection