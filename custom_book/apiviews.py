from rest_framework import generics

from .serializers import UserSerializer


class UserCreate(generics.CreateAPIView):
    """
    API utilizada para la creacion de usuarios
    """
    authentication_classes = ()
    permission_classes = ()
    serializer_class = UserSerializer