from django.apps import AppConfig


class CustomBookConfig(AppConfig):
    name = 'custom_book'
