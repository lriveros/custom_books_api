from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory

from custom_book import apiviews


class TestUser(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.view = apiviews.UserCreate.as_view({'get': 'list'})
        self.uri = '/login/'

    def test_list(self):
        request = self.factory.get(self.uri)
        response = self.view(request)
        self.assertEqual(response.status_code, 200,
                         'Expected Response Code 200, received {0} instead.'
                         .format(response.status_code))
