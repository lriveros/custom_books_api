<p align="center">
  <img src="./documentation/assets/images/logo.jpg" alt="Custom Books API" width=300 height=auto />
</p>

<p align="center">
	<b>Custom Books API</b>
</p>
  
<p align="center">
    This project provides a functional api environment.
</p>

## Content Table

* [General Description](#general-description)
* [Requirements](#requirements)
* [How to Setup?](#how-to-setup)
* [Change log](#change-log)

## General Description

This is the main backend custom books api.

## Requirements

* Python 3+
* Virtualenv
* Django framework

## How to Setup?

Run the main project with virtualenv recomended [SETUP.md](./documentation/SETUP.md)

## Change log

Every change is registried in this file [CHANGELOG.md](CHANGELOG.md), use it in your proyect, don´t forget it.

